<?php
/**
 * @file
 * A utility command for running specific update hooks manually.
 */

/**
 * Implements hook_drush_command().
 */
function upone_drush_command() {
  $items = array();

  $items['update-one'] = array(
    'description' => 'Manually execute specific update hooks - USE WITH CAUTION.',
    'examples' => array(
      'drush upone site_master 7301' => 'Executes update hook 7301 for the site_master module',
    ),
    'arguments' => array(
      'module' => 'The module containing the update hook to run.',
      'version' => 'The specific update hook schema number to run.',
    ),
    'aliases' => array(
      'upone',
    ),
  );

  return $items;
}

/**
 * Callback for update-one command.
 *
 * Runs a single specified update hook.
 *
 * @param string $module
 *   The Module machine name to update.
 * @param string $version
 *   The update schema version to run.
 */
function drush_upone_update_one($module, $version) {

  module_load_include('inc', 'content', 'includes/install');
  if (module_exists($module)) {
    module_load_install($module);
  }
  else {
    drush_print('The specified module ' . $module . ' does not exist or is disabled.');
    return;
  }

  // Get the current schema version and ensure that requested update can be run.
  $current_schema = drupal_get_installed_schema_version($module, FALSE, FALSE);
  $update_hook = $module . '_update_' . $version;
  if (!function_exists($update_hook)) {
    drush_print('The requested update hook does not exist in the install file for the module.');
    return;
  }
  if ($version == $current_schema) {
    drush_print('The module is currently at the requested version ' . $version);
    return;
  }
  if (($version - $current_schema) != 1) {
    drush_print('The update specified is too new to process with this command.  The current installed version is ' . $current_schema);
    return;
  }
  // Perform the requested update.
  drush_confirm('Run update ' . $version . ' for module ' . $module . '?', 3);
  drush_print('Running update ' . $update_hook);
  $update_hook();
  // Update the module schema.
  drupal_set_installed_schema_version($module, $version);
  // Check and report success.
  $new_version = drupal_get_installed_schema_version($module);
  if ($new_version != $version) {
    drush_print('Unable to set new module version ' . $version);
    return;
  }
  drush_print('Updated ' . $module . ' to version ' . $new_version);

}
